/**
 * Copyright (C) 2020 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: main.cpp
 * @brief Nexus WAMP router
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#include "config.h"
#include <CLI/CLI.hpp>
#include <spdlog/sinks/rotating_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

#include "router.hpp"

void show_version(size_t)
{
    std::cout << PACKAGE_STRING << std::endl;
    throw CLI::Success();
}

void init_logging(const std::string &filename, spdlog::level::level_enum log_level = spdlog::level::warn)
{
    // Create a file rotating logger with 5mb size max and 3 rotated files
    auto file_sink = std::make_shared<spdlog::sinks::rotating_file_sink_mt>(filename, 1024 * 1024 * 5, 3);
    file_sink->set_level(log_level);

    auto console_sink = std::make_shared<spdlog::sinks::stderr_color_sink_mt>();
    console_sink->set_level(log_level);

    std::vector<spdlog::sink_ptr> sinks{file_sink, console_sink};

    auto logger = std::make_shared<spdlog::logger>("logger", sinks.begin(), sinks.end());
    logger->set_level(log_level);
    logger->set_pattern("[%Y-%m-%d %H:%M:%S.%e] [%l] %v");

    spdlog::register_logger(logger);
}

int main(int argc, char **argv)
{

    CLI::App appargs{PACKAGE_NAME};

    unsigned port = 9001;
    std::string host = "0.0.0.0";
    std::string default_realm = "nexus";
    std::string api_uri = "http://localhost";
    std::string env_file = "/var/www/.env";
    std::string log_file = "/var/log/" PACKAGE_NAME ".log";
    std::string log_level = "warning";

    appargs.add_option("-P,--port", port, "WAMP port [default: " + std::to_string(port) + "]");
    appargs.add_option("-H,--host", host, "WAMP host address to serve the router on. [default " + host + "]");
    appargs.add_option("-R,--realm", default_realm, "WAMP default realm [default: " + default_realm + "]");
    appargs.add_option("-B,--backend", api_uri, "Application web api [default: " + api_uri + "]");
    appargs.add_option("-e,--backend-env", env_file, "Application web .env file [default: " + env_file + "]");
    appargs.add_option("-l,--log-file", log_file, "Log to file [default: " + log_file + "]");
    appargs.add_option("-L,--log-level",
                       log_level,
                       "Log level (trace, debug, info, warning, error, critical, off) [default: " + log_level + "]");
    appargs.set_config("-c,--config", "config.ini", "Read config from an ini file", false);
    appargs.add_flag_function("-v,--version", show_version, "Show version");

    try
    {
        appargs.parse(argc, argv);
    }
    catch (const CLI::ParseError &e)
    {
        return appargs.exit(e);
    }

    pthread_setname_np(pthread_self(), "main");

    init_logging(log_file, spdlog::level::from_str(log_level));

    Router router(host, port, default_realm, api_uri, env_file);
    return router.run();
}