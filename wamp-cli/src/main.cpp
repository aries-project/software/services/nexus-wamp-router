/**
 * Copyright (C) 2020 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: main.cpp
 * @brief Nexus WAMP router
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#include "config.h"
#include "wamp_client.hpp"
#include <CLI/CLI.hpp>
#include <nexus/utils/json.hpp>
#include <nexus/utils/string.hpp>
#include <iostream>
#include <wampcc/json.h>
#include <wampcc/wamp_session.h>

void show_version(size_t)
{
    std::cout << PACKAGE_STRING << std::endl;
    throw CLI::Success();
}

bool printValue(const wampcc::json_value &value, const std::string &prefix = "")
{
    if (value.is_null())
    {
    }
    else if (value.is_number() || value.is_real() || value.is_string())
    {
        std::cout << prefix << "=" << value << std::endl;
    }
    else if (value.is_array())
    {
        unsigned i = 0;
        for (auto &item : value.as_array())
        {
            printValue(item, prefix + "_" + std::to_string(i++));
        }
    }
    else if (value.is_object())
    {
        for (auto &pair : value.as_object())
        {
            auto suffix = pair.first;
            std::for_each(suffix.begin(), suffix.end(), [](char &c) { c = ::toupper(c); });
            printValue(pair.second, prefix + "_" + suffix);
        }
    }

    return true;
}

int main(int argc, char **argv)
{
    CLI::App appargs{PACKAGE_NAME};

    std::string uri = "ws://127.0.0.1:9001";
    std::string realm = "nexus";
    std::string env_file = "/var/www/.env";
    bool bash_compatible_output = false;
    std::string arg_list = "[]";
    std::string arg_dict = "{}";
    std::string procedure_name = "";
    std::string topic_name = "";
    std::string filter = "";
    std::string result_prefix = "RESULT";

    appargs.add_option("-u,--uri", uri, "WAMP default realm [default: " + uri + "]");
    appargs.add_option("-r,--realm", realm, "WAMP default realm [default: " + realm + "]");
    appargs.add_option("-e,--backend-env", env_file, "Web .env file [default: " + env_file + "]");

    appargs.require_subcommand(/* min */ 1, /* max */ 1);
    appargs.fallthrough(false);

    auto call = appargs.add_subcommand("call", "Call WAMP procedure");
    call->add_option("procedure", procedure_name, "WAMP procedure name")->required();
    call->add_option("--args-list", arg_list, "WAMP dict list [default: " + arg_list + "]");
    call->add_option("--args-dict", arg_dict, "WAMP dict args [default: " + arg_dict + "]");
    call->add_option("--filter", filter, "WAMP dict result filter [default: " + filter + "]");
    call->add_flag("-b,--bash",
                   bash_compatible_output,
                   "Generate bash compatible output [default: " + std::string(bash_compatible_output ? "on" : "off") +
                       "]");
    call->add_option("-p,--prefix", result_prefix, "Bash result prefix [default: " + result_prefix + "]");

    auto publish = appargs.add_subcommand("publish", "Publish WAMP message");
    publish->add_option("topic", topic_name, "WAMP procedure name")->required();
    publish->add_option("--args-list", arg_list, "WAMP dict list [default: " + arg_list + "]");
    publish->add_option("--args-dict", arg_dict, "WAMP dict args [default: " + arg_dict + "]");

    appargs.add_flag_function("-v,--version", show_version, "Show version");

    try
    {
        appargs.parse(argc, argv);
    }
    catch (const CLI::ParseError &e)
    {
        return appargs.exit(e);
    }

    wampcc::wamp_args args;

    {
        if (!arg_list.empty())
        {
            try
            {
                wampcc::json_value jv = wampcc::json_decode(arg_list.c_str(), arg_list.size());
                if (!jv.is_array())
                    throw std::runtime_error("expected JSON array");
                args.args_list = jv.as_array();
            }
            catch (std::exception &e)
            {
                throw std::runtime_error(std::string("invalid arglist parameter, ") + e.what());
            }
        }
        if (!arg_dict.empty())
        {
            try
            {
                auto jv = wampcc::json_decode(arg_dict.c_str(), arg_dict.size());
                if (!jv.is_object())
                    throw std::runtime_error("expected JSON object");
                args.args_dict = jv.as_object();
            }
            catch (std::exception &e)
            {
                throw std::runtime_error(std::string("invalid argdict parameter, ") + e.what());
            }
        }
    }

    nexus::Client cli(env_file);
    if (cli.connect(uri, realm))
    {

        if (*call)
        {
            auto fut = cli.call(procedure_name, args);
            auto result = fut.get();

            auto out = wampcc::json_object{
                {"was_error", result.was_error},
                {"error_uri", result.error_uri},
                {"args_list", result.args.args_list},
                {"args_dict", result.args.args_dict},
            };

            if (bash_compatible_output)
            {
                if (result.was_error)
                {
                    std::cout << "WAS_ERROR=" << (result.was_error ? "yes" : "no") << std::endl;
                    std::cout << "ERROR_URI=\"" << result.error_uri << "\"" << std::endl;
                }
                if (!filter.empty())
                {
                    auto filtered = nexus::json_utils::getProperty(result.args.args_dict, filter, "");
                    printValue(filtered, result_prefix);
                }
                else
                {
                    printValue(result.args.args_dict, result_prefix);
                    printValue(result.args.args_list, result_prefix);
                }

                if (result.was_error)
                    return 1;
            }
            else
            {
                std::cout << out << std::endl;
            }

            if (result.was_error)
                return 1;
        }
        else if (*publish)
        {
            cli.publish(topic_name, args);
        }
    }

    return 0;
}
