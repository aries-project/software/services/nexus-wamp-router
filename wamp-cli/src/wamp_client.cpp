#include "wamp_client.hpp"
#include <config.h>

#define MAX_WORKERS 1

namespace nexus
{

Client::Client(const std::string &env_file)
    : BackendService(PACKAGE_STRING, env_file, MAX_WORKERS, false)
{
}

void Client::onConnect()
{
    join(realm(), {"token"}, "nexus-client");
}

void Client::onDisconnect() {}

void Client::onJoin() {}

} // namespace nexus