#!/bin/bash

TARGET_USER="root"
TARGET_IP="fabui.local"
TARGET_DIR="/usr/bin"

if [ "x$1" != "x" ]; then
	TARGET_IP="$1"
fi

BUILD_DIR="."
RSYNC_ARGS="-a -e ssh"

APP=fabui-wamp-router
echo rsync $RSYNC_ARGS $BUILD_DIR/${APP}/src/${APP} $TARGET_USER@$TARGET_IP:$TARGET_DIR
rsync $RSYNC_ARGS $BUILD_DIR/${APP}/src/${APP} $TARGET_USER@$TARGET_IP:$TARGET_DIR

APP=fabui-gcode-service
echo rsync $RSYNC_ARGS $BUILD_DIR/${APP}/src/${APP} $TARGET_USER@$TARGET_IP:$TARGET_DIR
rsync $RSYNC_ARGS $BUILD_DIR/${APP}/src/${APP} $TARGET_USER@$TARGET_IP:$TARGET_DIR

APP=fabui-task-service
echo rsync $RSYNC_ARGS $BUILD_DIR/${APP}/src/${APP} $TARGET_USER@$TARGET_IP:$TARGET_DIR
rsync $RSYNC_ARGS $BUILD_DIR/${APP}/src/${APP} $TARGET_USER@$TARGET_IP:$TARGET_DIR

APP=fabui-monitor-service
echo rsync $RSYNC_ARGS $BUILD_DIR/${APP}/src/${APP} $TARGET_USER@$TARGET_IP:$TARGET_DIR
rsync $RSYNC_ARGS $BUILD_DIR/${APP}/src/${APP} $TARGET_USER@$TARGET_IP:$TARGET_DIR

PLUGIN=marlin-1.1.x
echo rsync $RSYNC_ARGS $BUILD_DIR/fabui-gcode-service/src/plugins/gcode/${PLUGIN}/lib${PLUGIN}.so $TARGET_USER@$TARGET_IP:$TARGET_DIR
rsync $RSYNC_ARGS $BUILD_DIR/fabui-gcode-service/src/plugins/gcode/${PLUGIN}/lib${PLUGIN}.so $TARGET_USER@$TARGET_IP:/usr/share/fabui/plugins/gcode/

PLUGIN=fablin-1.1.x
echo rsync $RSYNC_ARGS $BUILD_DIR/fabui-gcode-service/src/plugins/gcode/${PLUGIN}/lib${PLUGIN}.so $TARGET_USER@$TARGET_IP:$TARGET_DIR
rsync $RSYNC_ARGS $BUILD_DIR/fabui-gcode-service/src/plugins/gcode/${PLUGIN}/lib${PLUGIN}.so $TARGET_USER@$TARGET_IP:/usr/share/fabui/plugins/gcode/

PLUGIN=prusa-3.4.x
echo rsync $RSYNC_ARGS $BUILD_DIR/fabui-gcode-service/src/plugins/gcode/${PLUGIN}/lib${PLUGIN}.so $TARGET_USER@$TARGET_IP:$TARGET_DIR
rsync $RSYNC_ARGS $BUILD_DIR/fabui-gcode-service/src/plugins/gcode/${PLUGIN}/lib${PLUGIN}.so $TARGET_USER@$TARGET_IP:/usr/share/fabui/plugins/gcode/

