/**
 * Copyright (C) 2021 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: http_server.hpp
 * @brief HTTP server application class header
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#ifndef HTTP_SERVER_HPP
#define HTTP_SERVER_HPP

#include <cpp-httplib/httplib.h>
#include <string>
#include <chrono>
#include <wampcc/json.h>
#include <memory>

class HttpServer
{
public:
    HttpServer(const std::string &host = "0.0.0.0", unsigned port = 80, const std::string &static_path = "");

    bool run();

private:
    httplib::Server m_server;
    std::unique_ptr<httplib::Client> m_proxy_client;
    unsigned m_port;
    std::string m_host;

    wampcc::json_object generateToken(unsigned sub, const std::string &role, std::chrono::seconds expires_in);
};

#endif /* HTTP_SERVER_HPP */