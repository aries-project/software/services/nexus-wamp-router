#include "http_server.hpp"
#include <wampcc/json.h>
#include <jwt/jwt.hpp>

#define API_BASE_URL "/api/v1"

using namespace httplib;

HttpServer::HttpServer(const std::string &host, unsigned port, const std::string &static_path)
    : m_host(host), m_port(port), m_proxy_client(nullptr)
{

    m_server.set_keep_alive_max_count(1000);

    if (!static_path.empty())
    {
        m_server.set_mount_point("/", static_path.c_str());
    }

    m_server.Get("/", [=](const httplib::Request & /*req*/, httplib::Response &res) {
        res.set_redirect("/index.html");
    });

    m_server.Post(API_BASE_URL "/auth/token", [&](const httplib::Request &req, httplib::Response &res) {
        auto data = wampcc::json_decode(req.body.c_str());

        // std::cout << data["grant_type"] << std::endl;
        // std::cout << data["username"] << std::endl;
        // std::cout << data["password"] << std::endl;

        auto o = generateToken(1 /*user_id*/, "ADMIN" /* user role*/, std::chrono::seconds(60 * 60));
        res.set_content(wampcc::json_encode(o), "application/json");
    });

    m_server.Get(API_BASE_URL "/me", [](const httplib::Request &, httplib::Response &res) {
        wampcc::json_object user = {
            {"id", 1},
            {"email", "kesler.daniel@gmail.com"},
            {"name", "Daniel Kesler"},
            {"role", "ADMIN"}};
        res.set_content(wampcc::json_encode(user), "application/json");
    });

    m_server.set_pre_routing_handler([&](const auto &req, auto &res) {
        std::cout << "Pre-Routing: " << req.path << std::endl;
        std::cout << "  method: " << req.method << std::endl;
        return httplib::Server::HandlerResponse::Unhandled;
    });

    m_server.Get("/ws", [](const httplib::Request &req, httplib::Response &res) {
        //     /*
        //     GET /chat HTTP/1.1
        //     Host: example.com:8000
        //     Upgrade: websocket
        //     Connection: Upgrade
        //     Sec-WebSocket-Key: dGhlIHNhbXBsZSBub25jZQ==
        //     Sec-WebSocket-Version: 13
        //     */

        // std::cout << "GET @ /ws\n";

        if (req.get_header_value("Upgrade") == "websocket")
        {
            res.set_passthrough("localhost", 9001);
        }
        else
        {
            res.status = 400;
        }
    });
}

bool HttpServer::run()
{
    return m_server.listen(m_host.c_str(), m_port);
}

wampcc::json_object HttpServer::generateToken(unsigned sub, const std::string &role, std::chrono::seconds expires_in)
{
    using namespace jwt::params;
    std::string m_key = "secret_key";
    std::string m_alg = "hs256";

    auto iat_tp = std::chrono::system_clock::now();
    auto iat = std::chrono::duration_cast<std::chrono::seconds>(iat_tp.time_since_epoch()).count();

    auto exp_tp = iat_tp + expires_in;
    auto exp = std::chrono::duration_cast<std::chrono::seconds>(exp_tp.time_since_epoch()).count();
    auto exp_in = std::chrono::duration_cast<std::chrono::seconds>(expires_in).count();

    jwt::jwt_object obj{algorithm(m_alg), secret(m_key), payload({{"rle", role}})};

    obj.add_claim("iss", "nexus-jwt")
        .add_claim("sub", sub)
        .add_claim("iat", iat)
        .add_claim("exp", exp);

    std::string token = obj.signature();

    return {
        {"access_token", token},
        {"issued_at", iat},
        {"expires_in", exp_in},
        {"token_type", "bearer"}};
}